package com.devcamp.customerorderproductcrud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.customerorderproductcrud.model.ProductLine;

public interface ProductLineRepo extends JpaRepository<ProductLine, Integer> {

}
