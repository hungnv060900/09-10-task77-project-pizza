package com.devcamp.customerorderproductcrud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomerOrderProductCrudApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerOrderProductCrudApplication.class, args);
	}

}
