package com.devcamp.customerorderproductcrud.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.customerorderproductcrud.model.Office;

@Repository
public interface OfficeRepo extends JpaRepository<Office, Integer> {

}
