package com.devcamp.shoppizzacrud.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.shoppizzacrud.model.Customer;
import com.devcamp.shoppizzacrud.repository.ICustomerRepository;

@Service
public class CustomerService {
    @Autowired
    private ICustomerRepository customerRepository;

    public List<Customer> getAllCustomers() {
        return customerRepository.findAll();
    }

    public Optional<Customer> getCustomerById(int id) {
        return customerRepository.findById(id);
    }

    public Customer createCustomer(Customer Customer) {
        return customerRepository.save(Customer);
    }

    public Optional<Customer> updateCustomerById(int id, Customer updatedCustomer) {
        Optional<Customer> customerData = customerRepository.findById(id);

        if (customerData.isPresent()) {
            Customer customer = customerData.get();
            customer.setLastName(updatedCustomer.getLastName());
            customer.setFirstName(updatedCustomer.getFirstName());
            customer.setPhoneNumber(updatedCustomer.getPhoneNumber());
            customer.setAddress(updatedCustomer.getAddress());
            customer.setCity(updatedCustomer.getCity());
            customer.setState(updatedCustomer.getState());
            customer.setPostalCode(updatedCustomer.getPostalCode());
            customer.setCountry(updatedCustomer.getCountry());
            customer.setSalesRepEmployeeNumber(updatedCustomer.getSalesRepEmployeeNumber());
            customer.setCreditLimit(updatedCustomer.getCreditLimit());
            customer.setOrders(updatedCustomer.getOrders());
            customer.setPayments(updatedCustomer.getPayments());
            return Optional.of(customerRepository.save(customer));
        } else {
            return Optional.empty();
        }
    }

    public void deleteCustomerById(int id) {
        customerRepository.deleteById(id);
    }
}
