package com.devcamp.shoppizzacrud.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.shoppizzacrud.model.Employee;
import com.devcamp.shoppizzacrud.repository.IEmployeeRepository;

@Service
public class EmployeeService {
    @Autowired
    private IEmployeeRepository employeeRepository;

    public List<Employee> getAllEmployees() {
        return employeeRepository.findAll();
    }

    public Optional<Employee> getEmployeeById(int id) {
        return employeeRepository.findById(id);
    }

    public Employee createEmployee(Employee employee) {
        return employeeRepository.save(employee);
    }

    public Optional<Employee> updateEmployeeById(int id, Employee updatedEmployee) {
        Optional<Employee> employeeData = employeeRepository.findById(id);

        if (employeeData.isPresent()) {
            Employee employee = employeeData.get();
            employee.setLastName(updatedEmployee.getLastName());
            employee.setFirstName(updatedEmployee.getFirstName());
            employee.setExtension(updatedEmployee.getExtension());
            employee.setEmail(updatedEmployee.getEmail());
            employee.setOfficeCode(updatedEmployee.getOfficeCode());
            employee.setReportTo(updatedEmployee.getReportTo());
            employee.setJobTitle(updatedEmployee.getJobTitle());
            return Optional.of(employeeRepository.save(employee));
        } else {
            return Optional.empty();
        }
    }

    public void deleteEmployeeById(int id) {
        employeeRepository.deleteById(id);
    }
}
