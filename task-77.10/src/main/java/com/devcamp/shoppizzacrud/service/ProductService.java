package com.devcamp.shoppizzacrud.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.devcamp.shoppizzacrud.model.Product;
import com.devcamp.shoppizzacrud.model.ProductLine;
import com.devcamp.shoppizzacrud.repository.IProductLineReposiroty;
import com.devcamp.shoppizzacrud.repository.IProductRepository;

@Service
public class ProductService {
    @Autowired
    private IProductRepository productRepository;

    @Autowired
    private IProductLineReposiroty productLineRepository;

    public List<Product> getAllProducts() {
        return productRepository.findAll();
    }

    public Optional<Product> getProductById(int id) {
        return productRepository.findById(id);
    }

    public ResponseEntity<Object> createProduct(Integer productLineId, Product paramProduct) {
        Optional<ProductLine> vProductLineData = productLineRepository.findById(productLineId);
        if (vProductLineData.isPresent()) {
            try {
                Product vProduct = new Product();
                vProduct.setProductCode(paramProduct.getProductCode());
                vProduct.setProductName(paramProduct.getProductName());
                vProduct.setProductDescripttion(paramProduct.getProductDescripttion());
                vProduct.setProductLine(vProductLineData.get());
                vProduct.setProductScale(paramProduct.getProductScale());
                vProduct.setProductVendor(paramProduct.getProductVendor());
                vProduct.setQuantityInStock(paramProduct.getQuantityInStock());
                vProduct.setBuyPrice(paramProduct.getBuyPrice());
                Product vProductSave = productRepository.save(vProduct);
                return new ResponseEntity<>(vProductSave, HttpStatus.CREATED);
            } catch (Exception e) {
                return ResponseEntity.unprocessableEntity()
                        .body("Failed to Create specified Product: " + e.getCause().getCause().getMessage());
            }
        } else {
            ProductLine vProductLineNull = new ProductLine();
            return new ResponseEntity<>(vProductLineNull, HttpStatus.NOT_FOUND);
        }
    }

    public ResponseEntity<Object> updateProduct(Integer id, Integer productLineId, Product paramProduct) {
        Optional<Product> vProductData = productRepository.findById(id);
        if (vProductData.isPresent()) {
            Optional<ProductLine> vProductLineData = productLineRepository.findById(productLineId);
            if (vProductLineData.isPresent()) {
                try {
                    Product vProduct = vProductData.get();
                    vProduct.setProductCode(paramProduct.getProductCode());
                    vProduct.setProductName(paramProduct.getProductName());
                    vProduct.setProductDescripttion(paramProduct.getProductDescripttion());
                    vProduct.setProductLine(vProductLineData.get());
                    vProduct.setProductScale(paramProduct.getProductScale());
                    vProduct.setProductVendor(paramProduct.getProductVendor());
                    vProduct.setQuantityInStock(paramProduct.getQuantityInStock());
                    vProduct.setBuyPrice(paramProduct.getBuyPrice());
                    Product vProductSave = productRepository.save(vProduct);
                    return new ResponseEntity<>(vProductSave, HttpStatus.OK);
                } catch (Exception e) {
                    return ResponseEntity.unprocessableEntity()
                            .body("Failed to Update specified Product: " + e.getCause().getCause().getMessage());
                }
            } else {
                ProductLine vProductLineNull = new ProductLine();
                return new ResponseEntity<>(vProductLineNull, HttpStatus.NOT_FOUND);
            }
        } else {
            Product vProductNull = new Product();
            return new ResponseEntity<>(vProductNull, HttpStatus.NOT_FOUND);
        }
    }

    public void deleteProductById(int id) {
        productRepository.deleteById(id);
    }
}
