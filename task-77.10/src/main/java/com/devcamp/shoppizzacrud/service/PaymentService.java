package com.devcamp.shoppizzacrud.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.shoppizzacrud.model.Payment;
import com.devcamp.shoppizzacrud.repository.IPaymentRepository;

@Service
public class PaymentService {
    @Autowired
    private IPaymentRepository paymentRepository;

    public List<Payment> getAllPayments() {
        return paymentRepository.findAll();
    }

    public Optional<Payment> getPaymentById(int id) {
        return paymentRepository.findById(id);
    }

    public Payment createPayment(Payment payment) {
        return paymentRepository.save(payment);
    }

    public Optional<Payment> updatePaymentById(int id, Payment updatedPayment) {
        Optional<Payment> paymentData = paymentRepository.findById(id);

        if (paymentData.isPresent()) {
            Payment payment = paymentData.get();
            payment.setCheckNumber(updatedPayment.getCheckNumber());
            payment.setPaymentDate(updatedPayment.getPaymentDate());
            payment.setAmmount(updatedPayment.getAmmount());
            return Optional.of(paymentRepository.save(payment));
        } else {
            return Optional.empty();
        }
    }

    public void deletePaymentById(int id) {
        paymentRepository.deleteById(id);
    }
}
