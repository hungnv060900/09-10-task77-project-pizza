package com.devcamp.shoppizzacrud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.shoppizzacrud.model.Product;

public interface IProductRepository extends JpaRepository<Product, Integer> {
    
}
