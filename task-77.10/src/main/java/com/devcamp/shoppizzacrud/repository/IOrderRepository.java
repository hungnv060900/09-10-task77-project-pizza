package com.devcamp.shoppizzacrud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.shoppizzacrud.model.Order;

public interface IOrderRepository extends JpaRepository<Order, Integer> {
    
}
