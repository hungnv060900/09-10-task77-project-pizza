package com.devcamp.shoppizzacrud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.shoppizzacrud.model.Office;

public interface IOfficeRepository extends JpaRepository<Office, Integer> {
    
}
